#!/usr/bin/bash

Install nginx and the rtmp extension
sudo apt update
sudo apt install nginx
sudo apt install libnginx-mod-rtmp

# Create HLS output directory
sudo mkdir -p /mnt/hls

# write the nginx configuration file, substituting the current directory where needed.
cat > nginx.conf << EOF
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 768;
	# multi_accept on;
}
http {
	##
	# Basic Settings
	##
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 65;
	types_hash_max_size 2048;
	# server_tokens off;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;

	##
	# SSL Settings
	##
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	##
	# Logging Settings
	##
	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	##
	# Gzip Settings
	##
	gzip on;

	# Standard Web server - hosts the web player
	server {
	       include mime.types;
	       listen 80;
	       server_name stream-server;

	       location / {
	       	root $PWD;
		index index.html;
		}
	}

	# HLS server
	server {
		listen 8080;
		location / {
			add_header 'Cache-Control' 'no-cache';
			add_header 'Access-Control-Allow-Origin' '*' always;
			add_header 'Access-Control-Expose-Headers' 'Content-Length';
	
			if (\$request_method = 'OPTIONS') {
			   add_header 'Access-Control-Allow-Origin' '*';
			   add_header 'Access-Control-Max-Age' 1728000;
			   add_header 'Content-Type' 'text/plain charset=UTF-8';
			   add_header 'Content-Length' 0;
			   return 204;
			}
	
			types {
			      application/dash+xml mpd;
			      application/vnd.apple.mpegurl m3u8;
			      video/mp2t ts;
			}

			root /mnt/;
		}
	}
}

# RTMP Receive server with HLS support
rtmp {
	server {
		listen 1935;
		chunk_size 4096;

		application live {
			live on;
			# HLS Config
			hls on;
			hls_path /mnt/hls/;
			hls_fragment 2;
			hls_playlist_length 5;
			deny play all;
			record off;
		}
	}
}
EOF

# See if an IP address was provided as an argument
if [ $# -ge 1 ]; then
	ip_addr=$1
else
	# Get the IP address of this machine
	IFS=' ' read -ra ip_addrs <<< $(hostname -I)
	ip_addr=${ip_addrs[0]} # Use the first one by default
fi

echo Using IP Address $ip_addr

# Write the index.html file, substituting the IP of this machine where needed
cat > index.html << EOF
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Easy Streamer</title>
    <link href="https://vjs.zencdn.net/7.7.6/video-js.css" rel="stylesheet" />
</head>
<body>
    <video
    id="myvideo"
    class="video-js vjs-default-skin"
    controls
    width="1280"
    height="720">
        <source src="http://$ip_addr:8080/hls/magickey.m3u8" type="application/x-mpegURL" />
        <p class="vjs-no-js">
        To view this video please enable JavaScript, and consider upgrading to a
        web browser that
        <a href="https://videojs.com/html5-video-support/" target="_blank"
            >supports HTML5 video</a
        >
        </p>
    </video>

  <script src="https://vjs.zencdn.net/7.7.6/video.js"></script>
  <script src="https://vjs.zencdn.net/7.7.6/videojs.hls.min.js"></script>
  <script>
    var player=videojs('myvideo');
    player.play();
  </script>
</body>
</html>
EOF

# make a copy of the old nginx.conf file
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
# copy over the new config file
sudo cp ./nginx.conf /etc/nginx/nginx.conf

# restart the nginx server
sudo systemctl restart nginx.service

echo Installation complete

exit 0