# Easy Streamer

A super simple RTMP/HLS streaming server setup. Uses nginx to host a server which will take in an RTMP stream and expose it as a HLS stream. It also provides a web viewer using video.js.

This should go without saying, but I'll say it anyway: **DO NOT USE IN PRODUCTION**. This is little more than a patchwork of sample code. 

This project used these sites as resources:
* [https://opensource.com/article/19/1/basic-live-video-streaming-server](https://opensource.com/article/19/1/basic-live-video-streaming-server)
* [https://opensource.com/article/20/2/video-streaming-tools](https://opensource.com/article/20/2/video-streaming-tools)
* [https://docs.peer5.com/guides/setting-up-hls-live-streaming-server-using-nginx/](https://docs.peer5.com/guides/setting-up-hls-live-streaming-server-using-nginx/)

# Usage

Clone this repo on to an Ubuntu server.

## install_server.sh

This will install nginx and the rtmp extension. It also generates an nginx config file and an index.hml file to view the stream. The script will try to automatically determine the IP address of the machine. You can also provide the IP to use as an optional argument like so:

```bash
./install_server.sh 192.168.0.4
```

This is needed so the webviewer knows where to get the stream.

## createVirtualAudioSink.sh

This script creates a virtual audio sink using Pulse Audio. This is useful on the computer that is streaming content to the server. You route an application's audio to it and pick it up using OBS. This isolates the audio output of the stream to a single application rather than the entire computer. 

## OBS Setup
Set OBS to use a custom stream service and enter `rtmp://<server_ip>/live` as the server address. Use `magickey` as the stream key. Again, do not use in a production environment, this is not secure. You can use another stream key, just make sure to change line 15 in `index.html` to match. 

## Viewing the Stream
Point a web browser to the IP address of your server and click on the video box to start playback.